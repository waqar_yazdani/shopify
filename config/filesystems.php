<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "s3", "rackspace"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],
        'remote-sftp' => [
                'driver' =>  env('SFTP_C_Drive'),
                'host' => env('SFTP_C_HOST'),
                'port' => env('SFTP_C_PORT'),
                'username' => env('SFTP_C_USERNAME'),
                'password' => env('SFTP_C_PASSWORD'),
                'visibility' => 'public',
//                'privateKey' => env('PEM_KEY'),
                'root' => env('SFTP_C_ROOT'),
                'directoryPerm' => 0755, // whatever you want
                'timeout' => 30,
                'permPublic' => 0755,
            ],
//            'driver' => env('SFTP_C_Drive'),
//            'host' => env('SFTP_C_HOST'),
//            'port' => env('SFTP_C_PORT'),
//            'username' => env('SFTP_C_USERNAME'),
//            'password' => env('SFTP_C_PASSWORD'),
//            'visibility' => 'public', // set to public to use permPublic, or private to use permPrivate
//            'permPublic' => 0755, // whatever you want the public permission is, avoid 0777
//            'root' =>env('SFTP_C_ROOT'),
//            'timeout' => 30,
//            'directoryPerm' => 0755, // whatever you want
//            ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
        ],

    ],

];
