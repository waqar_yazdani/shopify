<?php

namespace App\Mail;

use Illuminate\Container\Container;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Mail\Mailable;
use Swift_Mailer;
use Swift_SmtpTransport;

class TriggerMailEvent extends Mailable
{
    public $shop;
    public $mail_settings;
    public $notification_body_text;
    public $notification_body_title;
    public $access_link;

    public function __construct( $notification_body_title, $notification_body_text,$mail_settings,$shop,$access_link)
    {

        $this->notification_body_title = $notification_body_title;
        $this->notification_body_text = $notification_body_text;
        $this->mail_settings = $mail_settings;
        $this->shop = $shop;
        $this->access_link = $access_link;
    }
    /**
     * Override Mailable functionality to support per-user mail settings
     *
     * @param  \Illuminate\Contracts\Mail\Mailer  $mailer
     * @return void
     */
    public function send(Mailer $mailer)
    {
        $host      = $this->mail_settings->mail_host;//new method I added on User Model
        $port      = $this->mail_settings->mail_port;//new method I added on User Model
        $security  = $this->mail_settings->mail_encryption;//new method I added on User Model

        //$transport = Swift_SmtpTransport::newInstance( $host, $port, $security);
        $transport = new Swift_SmtpTransport($host, $port, $security);
        $transport->setUsername($this->mail_settings->mail_username);//new method I added on User Model
        $transport->setPassword($this->mail_settings->mail_password);//new method I added on User Model
        $mailer->setSwiftMailer(new Swift_Mailer($transport));
        Container::getInstance()->call([$this, 'build']);
        $mailer->send($this->buildView(), $this->buildViewData(), function ($message) {
            $this->buildFrom($message)
                ->buildRecipients($message)
                ->buildSubject($message)
                ->buildAttachments($message)
                ->runCallbacks($message);
        });
    }

    public function build()
    {

            return $this->from($this->mail_settings->mail_from_address,$this->mail_settings->mail_from_name)
                ->subject('Shopify store webhook')
                ->view('mail.notification_template');

    }
}