<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductLogs extends Model
{
    protected $table='products_logs';
    protected $fillable=['facilityId','customerId','itemNumber','itemPart','itemLongDescription','itemShortDescription','grossWeight'];
}
