<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $table='customize_counter_setting_place';
    protected $fillable=['store_id','listing_page_class','detail_page_class','property_listing','property_detail','detail_icon_url','listing_icon_url'];
}
