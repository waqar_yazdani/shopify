<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Blogs extends Model
{
    protected $table='blogs';
    protected $fillable=['post_id ','blog_id','blog_title'];

    public function customer_blog_pivot(){
        return $this->hasMany(Customer_blog_pivot::class,'b_id','post_id');
    }
}
