<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Customer_blog_pivot extends Model
{
    protected $table='customer_blog_pivot';
    protected $fillable=['customer_id','b_id','is_register'];

    public function customer(){
        return $this->hasOne(Customer::class,'customer_id','customer_id');
    }
}
