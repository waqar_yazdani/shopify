<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class PostCounter extends Model
{
    protected $table='post_counter';
    protected $fillable = ['post_title', 'is_register','blog_id'];

}
