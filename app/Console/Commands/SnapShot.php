<?php

namespace App\Console\Commands;

use App\Entities\BoxLogs;
use App\Entities\Helper;
use App\Entities\OrderLogs;
use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Oseintow\Shopify\Facades\Shopify;
use SimpleXMLElement;
use PHPShopify\ShopifySDK;
class SnapShot extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedule:snapshotXML';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $shop = env('SFTP_C_STORENAME', 'dhlstore123');
        $token = session()->get('shopify_token');
        $query = array(
            "Content-type" => "application/json" // Tell Shopify that we're expecting a response in JSON format
        );
        $file_list = Storage::disk('remote-sftp')->allFiles('/out');
        $i = 1;
        foreach ($file_list as $key => $value) {
            if ("SNAPSHOT" == substr($value, 4, -35)) {
                //dd($value);
                $str = str_replace("out/", "", $value);
                $location = 'project/storage/app/public' . '/' . $str;
                $var = Storage::disk('public')->put(str_replace("out/", "", $value), Storage::disk('remote-sftp')->get($value));
                $xmls = new SimpleXMLElement(file_get_contents($location));
                $array = json_decode(json_encode($xmls));
                //dd($array->itemLines[0]->item->inventorySummaryLines->line);
                //$order_id = json_decode(json_encode($xmls))->orders->order->orderNumber;
                //dd($array->orderNumber);
                //dd($array);
                foreach ($array->itemLines as $key => $lines) {
                    //dd($lines);
                    $file_qty = 0;
                    //dd($lines);
                    //dd($lines->item->inventorySummaryLines);
                    //dd($lines->item->inventorySummaryLines);
                    $dsf = is_array($lines->item->inventorySummaryLines);
                    //dd($dsf);

                    if ($dsf == false) {
                        $file_qty = $file_qty + $lines->item->inventorySummaryLines->line->availableQuantity;
                        //dd($file_qty);
                        // $myarray = $lines->item->inventorySummaryLines;
                        $item_id = $lines->item->itemNumber;
                        $products = Helper::shopify_call($token, $shop, "/admin/api/2021-01/products/" . $item_id . ".json", array(), 'GET');
                        $products = $products['response'];
                        $pro = \GuzzleHttp\json_decode($products);
                        if ($pro->errors != 'Not Found') {
                            //return 1;
                            if (isset($pro->product->variants[0])){

                                $variants_id = $pro->product->variants[0]->id;
                                $inventory_item_id = $pro->product->variants[0]->inventory_item_id;
                                $shopify_inventry_qty = $pro->product->variants[0]->inventory_quantity;
                                $ans = $shopify_inventry_qty + $file_qty;
                                //dd($inventory_item_id);
                                $read_inventory = Helper::shopify_call($token, $shop, "/admin/api/2021-01/inventory_levels.json?inventory_item_ids=" . $inventory_item_id . "", array(), 'GET');
                                //dd($read_inventory);
                                $read_inventory_res = $read_inventory['response'];
                                $read_inventory_res = \GuzzleHttp\json_decode($read_inventory_res);
                                //dd($read_inventory_res);
                                $modify_data = array(
                                    "location_id" => $read_inventory_res->inventory_levels[0]->location_id,
                                    "inventory_item_id" => $read_inventory_res->inventory_levels[0]->inventory_item_id,
                                    "available" => $ans
                                );
                                $modified_product = Helper::shopify_call($token, $shop, "/admin/api/2021-01/inventory_levels/set.json", $modify_data, 'POST');

                            }
                        }
                    } else {
                         $myarray =$lines->item->inventorySummaryLines;

                        foreach ($myarray as $lines_qty) {
                            $file_qty = $file_qty + $lines_qty->line->availableQuantity;
                        }
                        //dd($file_qty);
                        $item_id = $lines->item->itemNumber;
                        $products = Helper::shopify_call($token, $shop, "/admin/api/2021-01/products/" . $item_id . ".json", array(), 'GET');
                        $products = $products['response'];
                        $pro = \GuzzleHttp\json_decode($products);
                       // dd($pro);
                        //dd($pro->errors);
                        if ( !isset($pro->errors)) {
                            //return 1;
                            if (isset($pro->product->variants[0])){

                               // dd($pro);
                                $variants_id = $pro->product->variants[0]->id;
                                $inventory_item_id = $pro->product->variants[0]->inventory_item_id;
                                $shopify_inventry_qty = $pro->product->variants[0]->inventory_quantity;
                                $ans = $shopify_inventry_qty + $file_qty;
                                //dd($inventory_item_id);
                                $read_inventory = Helper::shopify_call($token, $shop, "/admin/api/2021-01/inventory_levels.json?inventory_item_ids=" . $inventory_item_id . "", array(), 'GET');
                                //dd($read_inventory);
                                $read_inventory_res = $read_inventory['response'];
                                $read_inventory_res = \GuzzleHttp\json_decode($read_inventory_res);
                                //dd($read_inventory_res);
                                $modify_data = array(
                                    "location_id" => $read_inventory_res->inventory_levels[0]->location_id,
                                    "inventory_item_id" => $read_inventory_res->inventory_levels[0]->inventory_item_id,
                                    "available" => $ans
                                );
                                $modified_product = Helper::shopify_call($token, $shop, "/admin/api/2021-01/inventory_levels/set.json", $modify_data, 'POST');

                            }}
                        //dd($myarray);
                        // dd($myarray);


                        //dd($file_qty);

                        //dd('ok');
                    }
                }
                Storage::disk('remote-sftp')->delete($value);
            }
        }
    }
}
