<?php

namespace App\Console\Commands;

use App\Entities\boxLogs;
use App\Entities\OrderLogs;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use SimpleXMLElement;
class ANSConfirmation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'asn:confirmation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file_list = Storage::disk('remote-sftp')->allFiles('/out');
        foreach ($file_list as $key => $value) {
            if("ASNCONFIRM"==substr($value,4,-30)){
                $str=str_replace("out/","",$value);
                $location='project/storage/app/public'.'/'.$str;
                $var=Storage::disk('public')->put(str_replace("out/", "", $value), Storage::disk('remote-sftp')->get($value));
                $xmls = new SimpleXMLElement(file_get_contents($location));
                $array = json_decode(json_encode($xmls));
                if($array->orderNumber){
                    //$order_id = json_decode(json_encode($xmls))->orders->order->orderNumber;
                    //dd($array->orderNumber);
                    foreach ($array->receiptLines->line as $lines){
                        boxLogs::where('orderID',$array->orderNumber)->where('lineNumber',$lines->lineNumber)->update([
                            'receiveStatus'=>$lines->receiveStatus,
                            'receivedQuantity'=>$lines->receivedQuantity,
                            'originCode'=>$lines->originCode,
                        ]);
                    }
                    OrderLogs::where('order_id',$array->orderNumber)->update([
                        'messageDate'=>$array->messageDate,
                        'customerId'=>$array->customerId,
                        'receiptDate'=>$array->receiptDate,
                        'receiptNumber'=>$array->receiptNumber,
                        'receiptType'=>$array->receiptType,
                    ]);
                }
                Storage::disk('remote-sftp')->delete($value);

            }

        }
    }
}
