<?php

namespace App\Console\Commands;

use App\Entities\OrderLogs;
use App\Entities\ProductLogs;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use App\Entities\Helper;
use SimpleXMLElement;
class AdjustmentXML extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedule:AdjustmentXML';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $shop = env('SFTP_C_STORENAME','dhlstore123');
        $token = session()->get('shopify_token');
        $query = array(
            "Content-type" => "application/json" // Tell Shopify that we're expecting a response in JSON format
        );
        $file_list = Storage::disk('remote-sftp')->allFiles('/out');
        foreach ($file_list as $key => $value) {
            if("ADJUSTMENT"==substr($value,4,-35)){
                $str=str_replace("out/","",$value);
                $location='project/storage/app/public'.'/'.$str;
                $var=Storage::disk('public')->put(str_replace("out/", "", $value), Storage::disk('remote-sftp')->get($value));
                $xmls = new SimpleXMLElement(file_get_contents($location));
                $array = json_decode(json_encode($xmls));
                $order_id = json_decode(json_encode($xmls))->itemNumber;
                //$array->orderNumber;
                $product_id=ProductLogs::where('itemNumber',$order_id)->first()->customerId;
                $Inventory_item_id= Helper::shopify_call($token, $shop, "/admin/api/2021-01/products.json?ids=".$product_id."", array(), 'GET');
                $Inventory_item_id_res = $Inventory_item_id['response'];
                $Inventory_item_id_res = \GuzzleHttp\json_decode($Inventory_item_id_res);
                $inventory_item_id = $Inventory_item_id_res->products[0]->variants[0]->inventory_item_id;
                $location_id = Helper::shopify_call($token, $shop, "/admin/api/2021-01/inventory_levels.json?inventory_item_ids=".$inventory_item_id."", array(), 'GET');
                //dd($read_inventory);
                $location_id_res = $location_id['response'];
                $location_id_res = \GuzzleHttp\json_decode($location_id_res);
                $location_id = $location_id_res->inventory_levels[0]->location_id;
                //dd($location_id);
                $modify_data = array(
                    "location_id"=> $location_id,
                    "inventory_item_id"=> $inventory_item_id,
                    "available_adjustment"=> $array->adjustmentQuantity
                );
                $modified_product = Helper::shopify_call($token, $shop, "/admin/api/2021-01/inventory_levels/adjust.json", $modify_data, 'POST');
               // Storage::disk('remote-sftp')->delete($value);
            }

        }
    }
}
