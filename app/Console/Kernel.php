<?php

namespace App\Console;

use App\Entities\WebhookLog;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

//        $schedule->command('schedule:getXML')
//            ->everyMinute();
//
//        $schedule->command('asn:confirmation')
//            ->everyMinute();
//
//        $schedule->command('schedule:snapshotXML')
//            ->everyMinute();
//
//        $schedule->command('schedule:DoStatusChange')
//            ->everyMinute();
//
//        $schedule->command('schedule:AdjustmentXML')
//            ->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
