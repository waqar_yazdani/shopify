<?php

namespace App\Jobs;

use App\Services\ProcessSubscriptionContractWebhooksLib;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
class ProcessSubscriptionContractWebhooks implements ShouldQueue
{

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $tries = 1;
    public $timeout = 120;
    private $db_channel_config;
    private $shopify_request_data;
    private $shop;
    private $event_topic;
    private $event_name;
    private $shopify_request_header;

    public function __construct(
        $db_channel_config,
        $shopify_request_data,
        $shop, $event_topic,
        $shopify_request_header,
        $event_name
    )
    {

        $this->db_channel_config = $db_channel_config;
        $this->shopify_request_data = $shopify_request_data;
        $this->shop = $shop;
        $this->event_topic = $event_topic;
        $this->event_name = $event_name;
        $this->shopify_request_header = $shopify_request_header;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $trigger_webhook = new ProcessSubscriptionContractWebhooksLib(
            $this->db_channel_config,
            $this->shopify_request_data,
            $this->shop,
            $this->event_topic,
            $this->shopify_request_header,
            $this->event_name);
        $trigger_webhook->trigger();

    }
}
