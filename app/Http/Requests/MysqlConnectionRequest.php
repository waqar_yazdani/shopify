<?php

namespace App\Http\Requests;

class MysqlConnectionRequest extends JsonFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'host_name' => 'required',
            'port' => 'required',
            'database_name' => 'required',
            'username' => 'required'

        ];
        return $rules;
    }

    public function messages()
    {
        $error_messages =
            [
                'host_name.required' => "Host field is required.",
                'port.required' => "Port is required.",
                'database_name.required' => "Database name is required.",
                'username.required' => "Please provide username.",

            ];
        return $error_messages;
    }
}
