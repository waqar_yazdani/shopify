<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreFacebookPagePostEvent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'facebook_account_id' => 'required',
            'webhook_topic_id' => 'required',
            'channel_id' => 'required',
            'facebook_page_id' => 'required',
            'facebook_page_access_token' => 'required',

        ];
        return $rules;
    }

    public function messages()
    {
        $error_messages =
            [
                'facebook_page_id.required' => "Please choose facebook page to continue",
                'facebook_account_id.required' => "No facebook account details found",
                'channel_id.required' => "No channel details found.",
                'facebook_page_access_token.required' => "Invalid page access token.",

            ];
        return $error_messages;
    }
}
