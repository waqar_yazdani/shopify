<?php

namespace App\Http\Requests;

use App\Entities\GoogleContactEvent;
use Illuminate\Foundation\Http\FormRequest;

class StoreMailchimpSubscriberEvent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'mailchimp_account_id' => 'required',
            'webhook_topic_id' => 'required',
            'channel_id' => 'required',
            'channel_event_id' => 'required',
        ];

        return $rules;
    }

    public function messages()
    {
        $error_messages =
            [

                'channel_event_id.required' => "Please choose connector event type to continue.",
                'mailchimp_account_id.required' => "No hubspot account details found",
                'channel_id.required' => "No channel details found.",
                'webhook_topic_id.required' => "Choose topic to continue.",


            ];
        return $error_messages;
    }
}
