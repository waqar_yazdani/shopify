<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreSlackEvent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'webhook_topic_id' => 'required',
            'channel_id' => 'required',
//            'post_url' => 'required'
        ];
        return $rules;
    }

    public function messages()
    {
        $error_messages =
            [
//                'post_url.required' => "Please enter url to post webhooks.",
                'channel_id.required' => "No channel details found.",
                'webhook_topic_id.required' => "No event details found.",

            ];
        return $error_messages;
    }
}
