<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTwilioEvent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'webhook_topic_id' => 'required',
            'channel_id' => 'required',
            'twilio_phone_no' => 'required',
            'sid' => 'required',
            'receiving_phone_no' => 'required',
            'token' => 'required'
        ];
        return $rules;
    }

    public function messages()
    {
        $error_messages =
            [
                'twilio_phone_no.required' => "Please enter registered twilio phone number.",
                'receiving_phone_no.required' => "Please enter phone number to receive messages .",
                'sid.required' => "Please enter ACCOUNT SID.",
                'token.required' => "Please enter slack AUTH TOKEN.",
                'channel_id.required' => "No channel details found.",
                'webhook_topic_id.required' => "No event details found.",

            ];
        return $error_messages;
    }
}
