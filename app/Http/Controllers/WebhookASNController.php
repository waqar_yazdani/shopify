<?php

namespace App\Http\Controllers;

use App\Entities\IncomingShopifyWebhook;
use App\Entities\OrderLogs;
use App\Entities\Shop;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class WebhookASNController extends Controller
{
    public function ASN(Request $request)
    {

        try {
            $event_topic = $request->header('x-shopify-topic');
            $store_domain = $request->header('x-shopify-shop-domain');
            $shop = Shop::where('myshopify_domain', $store_domain)
                ->where('is_deleted', 0)
                ->first();
            //you will get data here
            $line_items = [];
            $numberIds=000;
            foreach ($request['line_items'] as $line_item) {
                $line_items['lineNumber'] = "00".$numberIds++;
                $line_items['itemNumber'] = $line_item['variant_id'];
                $line_items['orderedQuantity'] = $line_item['quantity'];
                $line_items['inventoryStatus'] = "A";
                $line_items['unitPrice'] = $line_item['price'];
                $line_items['unitPriceCurrency'] = $line_item['price_set']['shop_money']['currency_code'];
            }
            $data = [
                'facilityId'=>'E03',
                'customerId' => "9901127",
                'receiptNumber' => "receiptNumber",
                'receiptDate' => Carbon::parse($request['created_at'])->format('YmdHis'),
                'receiptType' => '',
                'supplierNumber' => '',
                'receiptLines' => '',
                'lineNumber'   => '',
                'itemNumber'   => '',
                'expectedQuantity' => '',
                ''
            ];
            $orderLogs = [
                'order_id' => $request['order_number'],
                'order_name' => $request['billing_address']['name'],
                'order_price' => $request['total_price'],
                'date_time' => Carbon::parse($request['created_at'])->format('YmdHis'),
                'customer_name' => $request['shipping_address']['name'],
                'xml_status_import' => '0',
                'xml_status_export' => '1',
                'json_array' => json_encode($data),
            ];
            $order = ASNLogs::create(
                $orderLogs
            );
            $check_not_duplicates = IncomingShopifyWebhook::handle_dublicate_webhooks($shop, $event_topic, $request->all());
            if ($check_not_duplicates) {
                $this->createXML($data, $order);
            }

        } catch (\Exception $e) {
            \Log::info('Incoming Webhook Exception Error:' . $e->getMessage()."Store Domain:".$store_domain??"");
        }
        return response()->json(array('success' => true, 'id' => $request->id, 'message' => "received"), 200);
    }
    public function xmlDownload(Request $request,$id){
        $file_list = Storage::disk('remote-sftp')->allFiles('/out');
        foreach ($file_list as $key => $value) {
            Storage::disk('public')->put(str_replace("out/", "", $value), Storage::disk('remote-sftp')->get($value));
            $str=str_replace("out/","",$value);
            OrderLogs::where('id',$id)->update([
                'DHL_created_file'=>$str
            ]);

            return redirect('project/storage/app/public'.'/'.$str);
        }
//        return redirect(public_path('storage/').'/'.$str);
    }

}
