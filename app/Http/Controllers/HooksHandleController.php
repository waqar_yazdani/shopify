<?php

namespace App\Http\Controllers;

use App\Entities\Blogs;
use App\Entities\Customer;
use App\Entities\Customer_blog_pivot;
use App\Entities\IncomingShopifyWebhook;
use App\Entities\OrderLogs;
use App\Entities\Settings;
use App\Entities\Shop;
use App\Entities\PostCounter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;

class HooksHandleController extends Controller
{

    public function CountView(Request $request)
    {
        $blog=Blogs::where('post_id',$request->article_id)->where('blog_id',$request->blog_id)->first();
        if(!$blog && !empty($request->article_id)){
            $data=[
                'post_id'=>$request->article_id,
                'blog_id'=>$request->blog_id,
                'blog_title'=>$request->articale_title,
            ];
            DB::table('blogs')->insert($data);
        }
        if($request->regitser_user != 0){
            $customer= Customer::where('customer_id',$request->user_id)->first();
            if(!$customer && !empty($request->user_id)){
                $data=[
                    'customer_id'=>$request->user_id,
                    'customer_name'=>$request->user_name,
                    'is_register'=>1,
                ];
                DB::table('customer')->insert($data);
            }
        }
        $dataPivot=[
           'customer_id'=>$request->user_id,
           'b_id'=>$request->article_id,
           'is_register'=>($request->user_id != 0)? '1' : '0' ,
           'updated_at'=>date('Y-m-d H:i:s'),
        ];

        Customer_blog_pivot::insert($dataPivot);



        return 1;

    }
    public function CountDisplay(Request $request)
    {
        $post_view = Customer_blog_pivot::where('b_id',$request->post_title_id)->count();
        $settings = Settings::first();
        $data =[
                'counter' =>$post_view,
                'setting'=>$settings
            ];
        return $data;
    }
}
