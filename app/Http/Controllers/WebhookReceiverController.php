<?php

namespace App\Http\Controllers;

use App\Entities\CustomerDataRequest;
use App\Entities\Helper;
use App\Entities\ProductLogs;
use App\Entities\Shop;
use App\Entities\boxLogs;
use App\Entities\OrderLogs;
use App\Entities\Tokens;
use App\Http\Requests\UploadXML;
use App\Mail\MailDHL;
use App\Mail\SendEmailOnAppInstall;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Filesystem;
use League\Flysystem\Sftp\SftpAdapter;
use Illuminate\Support\Facades\Response as FacadeResponse;
use App\Entities\IncomingShopifyWebhook;
use Illuminate\Support\Facades\Mail;
use SimpleXMLElement;

class WebhookReceiverController extends Controller
{
    private $webhook_logs;

    public function __construct()
    {

    }
    public
    function customers_redact(Request $request)
    {
        try {

            return response()->json(array('success' => true, 'shop_id' => $request->shop_id, 'message' => "received"), 200);

        } catch (\Exception $e) {

            //   \Log::info('error:' . $e->getMessage());
        }

        return response()->json(array('success' => true, 'message' => "received"), 200);

    }

    public
    function shop_redact(Request $request)
    {
        try {

            return response()->json(array('success' => true, 'shop_id' => $request->shop_id, 'message' => "received"), 200);

        } catch (\Exception $e) {

            //   \Log::info('error:' . $e->getMessage());
        }

        return response()->json(array('success' => true, 'message' => "received"), 200);

    }

    public
    function customers_data_request(Request $request)
    {
        try {
            $data = new CustomerDataRequest();
            $data->shop_id = $request->shop_id;
            $data->myshopify_domain = $request->shop_domain;
            $data->save();
            return response()->json(array('success' => true, 'shop_id' => $request->shop_id, 'message' => "We'll contact you shortly."), 200);

        } catch (\Exception $e) {
            return response()->json(array('success' => false, 'message' => "error processing your request."), 422);
        }
    }
    public function process_shopify_webhooks(Request $request)
    {
        try {
                $event_topic = $request->header('x-shopify-topic');
                $store_domain = $request->header('x-shopify-shop-domain');
                $shop = Shop::where('myshopify_domain', $store_domain)
                    ->where('is_deleted', 0)
                    ->first();
                //you will get data here
                $line_items = [];
                $numberIds=000;
                foreach ($request['line_items'] as $line_item) {
                    $line_items[]['line']=[
                            'lineNumber' => "00".++$numberIds,
                            'itemNumber' => $line_item['sku'],
                            'orderedQuantity' => $line_item['quantity'],
                            'inventoryStatus' => "A",
                            'unitPrice' => $line_item['price'],
                            'unitPriceCurrency' => $line_item['price_set']['shop_money']['currency_code'],
                        ];
                }
                $data = [
                    'facilityId'=>'E03',
                    'customerId' => "9901127",
                    'orderNumber' => $request['order_number'],
                    'orderDate' => Carbon::parse($request['created_at'])->format('YmdHis'),
                    'shipTo' => [
                        'addressLine1' => $request['shipping_address']['address1'],
                        "addressLine2" => "",//$request['shipping_address']['address2'],
                        "addressLine3" => "",//$request['shipping_address']['city'],
                        "postCode" => $request['shipping_address']['zip'],
                        "city" => $request['shipping_address']['city'],
//                        "state" => $request['shipping_address']['province'],
                        "countryCode" => $request['shipping_address']['country_code'],
                        "name" => $request['shipping_address']['name'],
                        "phoneNumber" => $request['shipping_address']['phone'],
                        "email" => $request['email'],
                    ],
                    "billTo" => [
                        'addressLine1' => $request['billing_address']['address1'],
                        "addressLine2" => "",//$request['billing_address']['address2'],
                        "addressLine3" => "",//$request['billing_address']['city'],
                        "postCode" => $request['billing_address']['zip'],
                        "city" => $request['billing_address']['city'],
//                        "state" => $request['billing_address']['province'],
                        "countryCode" => $request['billing_address']['country_code'],
                        "name" => $request['billing_address']['name'],
                        "phoneNumber" => $request['billing_address']['phone'],
                        "email" => $request['email'],
                    ],
                    "orderLines" => $line_items,
//                    [
//                        "line" => $line_items
//                    ]
                ];
                $orderLogs = [
                    'order_id' => $request['order_number'],
                    'order_name' => $request['id'],
                    'order_price' => $request['total_price'],
                    'date_time' => Carbon::parse($request['created_at'])->format('YmdHis'),
                    'customer_name' => $request['shipping_address']['name'],
                    'xml_status_import' => '0',
                    'xml_status_export' => '1',
                    'json_array' => json_encode($data),
                ];
                $order = OrderLogs::create(
                    $orderLogs
                );
            $check_not_duplicates = IncomingShopifyWebhook::handle_dublicate_webhooks($shop, $event_topic, $request->all());
            if ($check_not_duplicates) {
                $this->crudXML($data, $order,'create','DO_DE0700TOOAL_');
            }

        } catch (\Exception $e) {
            \Log::info('Incoming Webhook Exception Error:' . $e->getMessage()."Store Domain:".$store_domain??"");
        }
        return response()->json(array('success' => true, 'id' => $request->id, 'message' => "received"), 200);
    }


    public function xmlDownload(Request $request,$id){
        $file_list = Storage::disk('remote-sftp')->allFiles('/out');
        foreach ($file_list as $key => $value) {
            Storage::disk('public')->put(str_replace("out/", "", $value), Storage::disk('remote-sftp')->get($value));
            $str=str_replace("out/","",$value);
            OrderLogs::where('id',$id)->update([
                'DHL_created_file'=>$str
            ]);

            return redirect('project/storage/app/public'.'/'.$str);
        }
//        return redirect(public_path('storage/').'/'.$str);
    }
//    public function xmlDownloadCron(){
//        $file_list = Storage::disk('remote-sftp')->allFiles('out/');
//        foreach ($file_list as $key => $value) {
//            Storage::disk('public')->put(str_replace("out/", "", $value), Storage::disk('remote-sftp')->get($value));
//        }
//        return 1;
//    }
    public function UploadXML(UploadXML $request){
        $localFile = $request->file('uploadXML');
        $nameFile="DO_DE0700TOOAL_".Carbon::now()->format('dmYHis').'.xml';
        OrderLogs::where('id',$request->order_id)->update([
            'server_created_file'=>$nameFile
        ]);
        $filesystem = Storage::disk('remote-sftp');
        $filesystem->put('/in/'.$nameFile, $localFile);
        $filesystem->getDriver()->getAdapter()->setDirectoryPerm(0755);
        return redirect()->back();
    }
    public function itemCreate(Request $request){
        try {
            $shoped = env('SFTP_C_STORENAME','dhlstore123');
            $token = Tokens::where('id',1)->first()->accessToken;
            $query = array(
                "Content-type" => "application/json" // Tell Shopify that we're expecting a response in JSON format
            );
            $event_topic = $request->header('x-shopify-topic');
            $store_domain = $request->header('x-shopify-shop-domain');
            $shop = Shop::where('myshopify_domain', $store_domain)
                ->where('is_deleted', 0)
                ->first();
            //you will get data here
            $line_items = [];
            foreach ($request['variants'] as $req){
                $hsCode="";
                if(!is_null($req['inventory_item_id'])){
                    $hsCode = Helper::shopify_call($token, $shoped, "/admin/api/2020-01/inventory_items/".$req['inventory_item_id'].".json", array(), 'GET');
                    $hsCode = $hsCode['response'];
                    $hsCode = \GuzzleHttp\json_decode($hsCode);
                    $hsCode = $hsCode->inventory_item->harmonized_system_code;
                }
               // \Log::info($hsCode);
              //  dd();
            $data = [
                'facilityId'=>'E03',
                'customerId' => "9901127",
                'itemNumber' => $req['sku'],
                'itemPart' => [
                    'itemLongDescription' => $request['title'],
                    'itemShortDescription' => $request['title'],
                    'originCodeFlag' => "TRUE",
                    'hazardousMaterialFlag' => "FALSE",
                    'commodityCode'=> $hsCode,
                ],
                'alternateParts'=>[
                    'alternatePart' => [
                        'alternateItemNumber' => ($req['barcode']) ? $req['barcode'] : "-",
                        'alternateItemType' => "EAN",
                    ],
                    ],
                'footprint'=>[
                    'footprintCode' => "STD",
                    'longDescription' => "STD",
                    'footprintDetails'=>[
                        'footprintDetail'=>[
                            "footprintCode"=>'STD',
                            "uomCode"=>'EA',
                            "unitUOMFlag"=>'TRUE',
                            "caseFlag"=>'FALSE',
                            "palletFlag"=>'FALSE',
                            "uomQuantity"=>'1',
                            "grossWeight"=>$req['weight'],
                            "length"=>'1',
                            "width"=>'1',
                            "height"=>'1',
                        ]
                    ]
                ]
            ];
                $orderLogs = [
                    'facilityId'=>'E03',
                    'customerId' => $req['product_id'],
                    'itemNumber' => ($req['sku'])?  $req['sku'] : "-",
                    'itemPart' => $req['id'],
                    'itemLongDescription' => ($req['sku']) ? $req['sku'] : "-",
                    'itemShortDescription' => ($req['sku'])? $req['sku']  : "-",
                    'grossWeight' => $req['weight'],
                ];
                $order = ProductLogs::create(
                    $orderLogs
                );
                $check_not_duplicates = IncomingShopifyWebhook::handle_dublicate_webhooks($shop, $event_topic, $request->all());
                if ($check_not_duplicates) {
                    $this->createXMLProduct($data, $order);
                }
            }


        } catch (\Exception $e) {
            \Log::info('Incoming Webhook Exception Error:' . $e->getMessage()."Store Domain:".$store_domain??"");
        }
        return response()->json(array('success' => true, 'id' => $request->id, 'message' => "received"), 200);
    }
    public function createXMLProduct($array,$order)
    {
        $output = \Illuminate\Support\Facades\View::make('ItemMasterXML')->with(compact('array'))->render();
        $xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> \n" .$output;
        $nameFile="ITEM_DE0700TOOAL_".Carbon::now()->format('dmYHis').'_'.rand(111,999).'.xml';
        Storage::put( 'public/in/'.$nameFile, $xml);
        ProductLogs::where('id',$order->id)->update([
                'server_created_file'=>$nameFile
            ]
        );
        $filesystem = Storage::disk('remote-sftp');
        $filesystem->put('/in/'.$nameFile, $xml);
        $filesystem->getDriver()->getAdapter()->setDirectoryPerm(0755);
        return $xml;
    }
    public function orderCancellation(Request $request){

     try {
         $event_topic = $request->header('x-shopify-topic');
         $store_domain = $request->header('x-shopify-shop-domain');
         $shop = Shop::where('myshopify_domain', $store_domain)
             ->where('is_deleted', 0)
             ->first();
         //you will get data here
         $line_items = [];
         $numberIds=000;
         foreach ($request['line_items'] as $line_item) {
             $line_items[]['line']=[
                 'lineNumber' => "00".++$numberIds,
                 'itemNumber' => $line_item['variant_id'],
                 'orderedQuantity' => $line_item['quantity'],
                 'inventoryStatus' => "A",
                 'unitPrice' => $line_item['price'],
                 'unitPriceCurrency' => $line_item['price_set']['shop_money']['currency_code'],
             ];
         }
         $data = [
             'facilityId'=>'E03',
             'customerId' => "9901127",
             'orderNumber' => $request['order_number'],
             'cancelledFlag' => "TRUE",
             'orderDate' => Carbon::parse($request['created_at'])->format('YmdHis'),
             'shipTo' => [
                 'addressLine1' => $request['shipping_address']['address1'],
                 "addressLine2" => "",//$request['shipping_address']['address2'],
                 "addressLine3" => "",//$request['shipping_address']['city'],
                 "postCode" => $request['shipping_address']['zip'],
                 "city" => $request['shipping_address']['city'],
    //                        "state" => $request['shipping_address']['province'],
                 "countryCode" => $request['shipping_address']['country_code'],
                 "name" => $request['shipping_address']['name'],
                 "phoneNumber" => $request['shipping_address']['phone'],
                 "email" => $request['email'],
             ],
             "billTo" => [
                 'addressLine1' => $request['billing_address']['address1'],
                 "addressLine2" => "",//$request['billing_address']['address2'],
                 "addressLine3" => "",//$request['billing_address']['city'],
                 "postCode" => $request['billing_address']['zip'],
                 "city" => $request['billing_address']['city'],
    //                        "state" => $request['billing_address']['province'],
                 "countryCode" => $request['billing_address']['country_code'],
                 "name" => $request['billing_address']['name'],
                 "phoneNumber" => $request['billing_address']['phone'],
                 "email" => $request['email'],
             ],
             "orderLines" => $line_items,
    //                    [
    //                        "line" => $line_items
    //                    ]
         ];
         $orderLogs = [
             'order_id' => $request['order_number'],
             'order_name' => $request['id'],
             'order_price' => $request['total_price'],
             'date_time' => Carbon::parse($request['created_at'])->format('YmdHis'),
             'customer_name' => $request['shipping_address']['name'],
             'xml_status_import' => '0',
             'xml_status_export' => '1',
             'json_array' => json_encode($data),
         ];
         $order = OrderLogs::create(
             $orderLogs
         );
         $check_not_duplicates = IncomingShopifyWebhook::handle_dublicate_webhooks($shop, $event_topic, $request->all());
         if ($check_not_duplicates) {
             $this->crudXML($data, $order,'create','DO_DE0700TOOAL_');
         }

     } catch (\Exception $e) {
         \Log::info('Incoming Webhook Exception Error:' . $e->getMessage()."Store Domain:".$store_domain??"");
     }
        return response()->json(array('success' => true, 'id' => $request->id, 'message' => "received"), 200);
    }
    public function createXMLCancel($array,$order)
    {
        $output = \Illuminate\Support\Facades\View::make('xml')->with(compact('array'))->render();
        $xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> \n" .$output;
        $nameFile="DO_DE0700TOOAL_DELETE".Carbon::now()->format('dmYHis').'.xml';
        Storage::put( 'public/in/'.$nameFile, $xml);
//        OrderLogs::where('id',$order->id)->update([
//                'server_created_file'=>$nameFile
//            ]
//        );
        $filesystem = Storage::disk('remote-sftp');
        $filesystem->put('/in/'.$nameFile, $xml);
        $filesystem->getDriver()->getAdapter()->setDirectoryPerm(0755);
//        Mail::to('dhl@dhl.com')->send(new MailDHL($nameFile));

        return $xml;
    }
    public function createXML($array,$order)
    {
        $output = \Illuminate\Support\Facades\View::make('xml')->with(compact('array'))->render();
        $xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> \n" .$output;
        $nameFile="DO_DE0700TOOAL_".Carbon::now()->format('dmYHis').'.xml';
        Storage::put( 'public/in/'.$nameFile, $xml);
        OrderLogs::where('id',$order->id)->update([
                'server_created_file'=>$nameFile
            ]
        );
        $filesystem = Storage::disk('remote-sftp');
        $filesystem->put('/in/'.$nameFile, $xml);
        $filesystem->getDriver()->getAdapter()->setDirectoryPerm(0755);
        Mail::to('dhl@dhl.com')->send(new MailDHL($nameFile));

        return $xml;
    }

    public function crudXML($array,$order,$action,$xmlfile)
    {
        if($action=="createXMLProduct"){
            $output = \Illuminate\Support\Facades\View::make('ItemMasterXML')->with(compact('array'))->render();
        }
        else{
            $output = \Illuminate\Support\Facades\View::make('xml')->with(compact('array'))->render();
        }
        $xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> \n" .$output;
        $nameFile=$xmlfile.Carbon::now()->format('dmYHis').'.xml';
        Storage::put( 'public/in/'.$nameFile, $xml);
        if ($action == 'create'){
            OrderLogs::where('id',$order->id)->update([
                    'server_created_file'=>$nameFile
                ]
            );
        }elseif($action == 'createXMLProduct'){
            ProductLogs::where('id',$order->id)->update([
                    'server_created_file'=>$nameFile
                ]
            );
        }
        $filesystem = Storage::disk('remote-sftp');
        $filesystem->put('/in/'.$nameFile, $xml);
        $filesystem->getDriver()->getAdapter()->setDirectoryPerm(0755);
        if ($action == 'create'){
            Mail::to('dhl@dhl.com')->send(new MailDHL($nameFile));

        }
        return $xml;
    }



}
