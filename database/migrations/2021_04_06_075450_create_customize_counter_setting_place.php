<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomizeCounterSettingPlace extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customize_counter_setting_place', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('store_id')->nullable();
            $table->string('listing_page_class')->nullable();
            $table->string('detail_page_class')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customize_counter_setting_place');
    }
}
