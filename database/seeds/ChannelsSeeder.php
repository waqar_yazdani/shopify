<?php

use Illuminate\Database\Seeder;

class ChannelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
         /*   [
                'channel_id' => 1,
                'name' => 'slack',
                'details' => 'Forward shopify webhooks to slack incoming webhooks',
                'icon_path'=>URL::to('/') .'/icons/slack.png',
                'status'=>1

            ],
            [
                'channel_id' => 2,
                'name' => 'google_sheets',
                'details' => 'Forward shopify webhooks to google sheets',
                'icon_path'=>URL::to('/') .'/icons/google_sheets.png',
                     'status'=>1
            ],
            [
                'channel_id' => 3,
                'name' => 'gmail',
                'details' => 'Forward shopify webhooks to gmail',
                'icon_path'=>URL::to('/') .'/icons/gmail.png',
            'status'=>0
            ],
            [
                'channel_id' => 4,
                'name' => 'twilio',
                'details' => 'Forward shopify webhooks to twilio',
                'icon_path'=>URL::to('/') .'/icons/twilio.png',
            'status'=>1
            ],
            [
                'channel_id' => 5,
                'name' => 'mail',
                'details' => 'Shopify webhooks to your email',
                'icon_path'=>URL::to('/') .'/icons/mail.png',
            'status'=>1
            ],
            [
                'channel_id' => 6,
                'name' => 'post_to_url',
                'details' => 'Shopify webhooks to your server or custom crm',
                'icon_path'=>URL::to('/') .'/icons/post_to_url.png',
            'status'=>1
            ],
            [
                'channel_id' => 7,
                'name' => 'web_push_notification',
                'details' => 'Shopify webhooks to your web notifications',
                'icon_path'=>URL::to('/') .'/icons/web_push_notification.png',
            'status'=>1
            ],
            [
                'channel_id' => 8,
                'name' => 'microsoft_excel_sheet',
                'details' => 'Shopify webhooks to your microsoft excel sheet',
                'icon_path'=>URL::to('/') .'/icons/microsoft_excel_sheet.png',
            'status'=>1
            ]

,

            [
                'channel_id' => 9,
                'name' => 'mysql',
                'details' => 'Save shopify data to your mysql database',
                'icon_path'=>URL::to('/') .'/icons/mysql.png',
                'status'=>1
            ],
            [
                'channel_id' => 10,
                'name' => 'facebook',
                'details' => 'Forward shopify data to your facebook account',
                'icon_path'=>URL::to('/') .'/icons/facebook.png',
                'status'=>1
            ],

            [
                'channel_id' => 11,
                'name' => 'twitter',
                'details' => 'Forward shopify data to your twitter account',
                'icon_path'=>URL::to('/') .'/icons/twitter.png',
                'status'=>1
            ],
            [
            'channel_id' => 12,
            'name' => 'google_contacts',
            'details' => 'Add shopify data to your google contacts list',
            'icon_path'=>URL::to('/') .'/icons/google_contacts.png',
            'status'=>1
        ],
            [
                'channel_id' => 13,
                'name' => 'hubspot',
                'details' => 'Forward shopify data to your hubspot CRM.',
                'icon_path'=>URL::to('/') .'/icons/hubspot.png',
                'status'=>1
            ],
            [
                'channel_id' => 14,
                'name' => 'mailchimp',
                'details' => 'Forward shopify data to your mailchimp.',
                'icon_path'=>URL::to('/') .'/icons/mailchimp.png',
                'status'=>1
            ],
            [
                'channel_id' => 15,
                'name' => 'salesforce',
                'details' => 'Forward shopify data to your salesforce.',
                'icon_path'=>URL::to('/') .'/icons/salesforce.png',
                'status'=>1
            ], */

            [
                'channel_id' => 16,
                'name' => 'quickBooks_online',
                'details' => 'Forward shopify data to your QuickBooks.',
                'icon_path'=>URL::to('/') .'/icons/quickbooks.png',
                'status'=>1
            ]
        ];

        \App\Entities\Channel::insert($data);
    }
}
