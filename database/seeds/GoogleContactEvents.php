<?php

use Illuminate\Database\Seeder;

class GoogleContactEvents extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [

                'name' => 'Add Contact',
                'slug' => 'create_contact',
                'status'=>1
            ]
           , [

                'name' => 'Update Contact',
                'slug' => 'update_contact',
                'status'=>1
            ]
            , [

                'name' => 'Upload Contact Photo',
                'slug' => 'upload_contact_photo',
                'status'=>1
            ]
        ];
        \App\Entities\GoogleContactEvent::insert($data);
    }
}
