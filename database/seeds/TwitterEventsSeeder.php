<?php

use Illuminate\Database\Seeder;

class TwitterEventsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [

                'name' => 'Create Tweet',
                'slug' => 'create_tweet',
                'status'=>1
            ]
            //this is not active yet (need to write its code)
//           , [
//
//                'name' => 'Update Page Post',
//                'slug' => 'update_page_post',
//                'status'=>1
//            ]
        ];
        \App\Entities\TwitterEvent::insert($data);
    }
}
