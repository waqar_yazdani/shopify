<?php  
	return
	[
		'blog_id' => 'ブログID',
		'post_id' => '投稿ID',
		'post_title' => '記事のタイトル',
		'vistor_name' => '訪問者名',
		'email' => 'Eメール',
		'total_spent' => '総支出',
		'total_guest_user' => 'ゲストユーザーの合計',
		'total_register_user' => '登録ユーザー総数',
		'total_visitor'=>'総訪問者',
		'sr'=>'Sr',
        'last_view' =>'最後のビュー',
        'total_user'=>"総ユーザー数",
        "register_user"=>"ユーザーの登録",
        "gu_user"=>"ゲストユーザー",
        "dashboard_details"=>"ダッシュボードの詳細",
        'vistor_list'=>"訪問者リスト",
        'settings'=>"設定",
        'listing_page'=>'リストページ',
        'detail_page'=>"詳細ページ",
        'blog_name'=>"ブログ名",
        'counter'=>'カウンター',
        'back'=>'バック',
    ];
?>