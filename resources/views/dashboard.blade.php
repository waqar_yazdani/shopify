@extends('layouts.master')
@section('content')
    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.4.1/css/buttons.dataTables.min.css">
    <style>
        .content {
            text-align: center;
        }
    </style>
    <div class="dropdown" >
        <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">LANG
            <span class="caret"></span></button>
        <ul class="dropdown-menu">
            <li> <a href="{{url('locale/en')}}">EN</a> </li>
            <li> <a href="{{url('locale/ch')}}">JP</a> </li>
        </ul>
    </div>
    <div id="default_modal" class="modal fade" data-backdrop="static">
    </div>

    <div class="col-lg-12">
        <div class="row">

            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item active">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">{{__('msg.dashboard_details')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">{{__('msg.settings')}}</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade  active in" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="container" style="width: auto !important; height: auto !important;">
                                <div class="row">
                                    <table id="vistor" class="display" style="width:100%">
                                        <thead>
                                        <tr>
                                            <th>{{__('msg.blog_name')}}</th>
                                            <th>{{__('msg.total_guest_user')}}</th>
                                            <th>{{__('msg.total_register_user')}}</th>
                                            <th>{{__('msg.total_visitor')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(!empty($data))
                                            @foreach($data as $mydata)
                                                <tr>
                                                    <td>{{ $mydata['title'] }}</td>
                                                    <td>{{ $mydata['guest_user'] }}</td>
                                                    <td><a href="{{ url("post/".$mydata['post_id']."/".$mydata['title']) }}">{{ $mydata['register_user'] }}</a></td>
                                                    <td>{{ $mydata['total_user'] }}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade  in" id="profile" role="tabpanel" aria-labelledby="home-tab">
                    <div class="row">
                        <div class="col-lg-4">
                            <form method="post" action="{{url('settings_counter')}}">

                                <div class="form-group">
                                    <label for="listing_page">{{__('msg.listing_page')}}</label>
                                    <input type="text" name="listing_class" class="form-control" value="{{$settings->listing_page_class}}" id="listing_page" placeholder="Enter Listing Page Class">
                                    <select name="listing_page_options" class="form-control">
                                        <option value="class">Class</option>
                                        <option value="id">ID</option>
                                    </select>
                                    <input type="text" name="listing_icon_url" class="form-control" value="{{$settings->listing_icon_url}}" id="listing_icon_url" placeholder="Enter Icon Url">
                                </div>
                                <div class="form-group">
                                    <label for="detail_page">{{__('msg.detail_page')}}</label>
                                    <input type="text" name="detail_class" class="form-control" value="{{$settings->detail_page_class}}" id="detail_page" placeholder="Enter Detail Page">
                                    <select name="detial_page_options" class="form-control">
                                        <option value="class">Class</option>
                                        <option value="id">ID</option>
                                    </select>
                                    <input type="text" name="detail_icon_url" class="form-control" value="{{$settings->detail_icon_url}}" id="detail_icon_url" placeholder="Enter Icon Url">
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

@section('last_scripts')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/buttons.flash.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/buttons.html5.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/buttons.print.min.js"></script>
    <script>
        $(document).ready(function() {

            $('#table').DataTable({
                dom: 'lBfrtip',
                destroy: true,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            } );
            $('#vistor').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });


        } );
    </script>

@endsection


