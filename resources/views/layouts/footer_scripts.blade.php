<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>

<script src="{{ asset('js/helper_functions.js') }}" type="text/javascript"></script>
@if(isset($asset_controls) && !empty($asset_controls))
    <?php
    foreach(array_unique($asset_controls) as $asset):
    if(!empty($components = get_asset_components($asset, 'js'))){
    if(isset($components['before-appjs']) && !empty($components['before-appjs'])){
    foreach($components['before-appjs'] as $component):
    ?>
    <script src="{{ URL::asset($component) }}" type="text/javascript"></script>
    <?php  endforeach;
    }
    }
    else{
    if(!empty($asset) && strpos('.js', $asset)){?>
    <script src="{{ URL::asset($asset) }}" type="text/javascript"></script>
    <?php } }
    endforeach;
    ?>
@endif

@if(isset($asset_controls) && !empty($asset_controls))
    <?php foreach(array_unique($asset_controls) as $asset):
    if(!empty($components = get_asset_components($asset, 'js'))){
    if(isset($components['after-appjs']) && !empty($components['after-appjs'])){
    foreach($components['after-appjs'] as $component):
    ?>
    <script src="{{ URL::asset($component) }}" type="text/javascript"></script>
    <?php  endforeach;
    }
    }

    endforeach;?>
@endif
<script type="text/javascript">

    $(document).ready(function () {
        $(document).on("mouseenter", ".connectify-tooltip-container", function () {
            var eTop = $(this).offset().top;
            var scroll_top = eTop - $(window).scrollTop();
            var tool_tip_contect_div = $(this).find('.connectify-tooltip');
            if (scroll_top < tool_tip_contect_div.outerHeight()) {
                tool_tip_contect_div.addClass("pos-bottom");
            } else {
                tool_tip_contect_div.removeClass("pos-bottom");
            }
        });
    });
</script>

